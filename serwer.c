#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <inttypes.h>
#include <dirent.h>
#include <malloc.h>
#include <fcntl.h>
#include <sys/sendfile.h>
#include <sys/stat.h>
#include <stdlib.h>

#include "err.h"
#include "netstore.h"

#define QUEUE_LENGTH     5
#define PORT_NUM     6543
#define MAX_PATH 4096

bool is_file(const char* path, const char* file_path) {
    static char complete_filename[MAX_PATH];
    complete_filename[0] = '\0';
    strcpy(complete_filename, path);
    strcat(complete_filename, file_path);
    struct stat statbuf;
    stat(complete_filename, &statbuf);
    return S_ISREG(statbuf.st_mode);
}

void listdir(const char* path, char** result, uint32_t* length) {
    DIR* dir = opendir(path);
    struct dirent* entry = readdir(dir);
    *result = NULL;
    static char pipe = '|';
    *length = 0;

    while (entry != NULL) {
        size_t entry_len = strlen(entry->d_name);
        if (is_file(path, entry->d_name)) {
            *length = *length + 1 + entry_len;
        }
        entry = readdir(dir);
    }

    closedir(dir);

    *result = malloc(*length);
    if (*result == NULL) {
        return;
    }
    dir = opendir(path);
    entry = readdir(dir);
    uint32_t current_pos = 0;
    while (entry != NULL) {
        size_t entry_len = strlen(entry->d_name);
        if (is_file(path, entry->d_name)) {
            memcpy(*result + current_pos, entry->d_name, entry_len);
            current_pos += entry_len;
            memcpy(*result + current_pos, &pipe, 1);
            current_pos += 1;
        }
        entry = readdir(dir);
    }
    closedir(dir);
}

bool handle_filenames_request(int socket_fd, char* path) {
    char* filenames;
    uint32_t filenames_length;
    listdir(path, &filenames, &filenames_length);
    if (filenames == NULL) {
        return false;
    }
    printf("got file list\n");

    // get rid of remaining pipe
    if (filenames_length > 0) {
        filenames_length--;
    }

    if (!write_exactly_short(socket_fd, RESPONSE_TYPE_LIST)
        || !write_exactly_long(socket_fd, filenames_length)
        || !write_exactly(socket_fd, filenames, filenames_length)) {
        free(filenames);
        return false;
    }

    free(filenames);
    return true;
}

bool handle_fragment_request(int socket_fd, char* path) {
    uint32_t fragment_start;
    uint32_t fragment_length;
    uint16_t filename_length;

    if (!read_exactly_long(socket_fd, &fragment_start)
        || !read_exactly_long(socket_fd, &fragment_length)
        || !read_exactly_short(socket_fd, &filename_length)) {
        return false;
    }

    char* filename = malloc(filename_length + 1);
    if (filename == NULL) {
        return false;
    }
    if (!read_exactly(socket_fd, filename, filename_length)) {
        free(filename);
        return false;
    }

    filename[filename_length] = '\0';
    printf("fragment of file %.*s was requested, bytes from %u to %u\n",
           filename_length, filename, fragment_start, fragment_length);

    if (strstr(filename, "/") != NULL || !is_file(path, filename)) {
        printf("client tried to request a wrong file \n");
        write_exactly_short(socket_fd, RESPONSE_TYPE_REJECT);
        write_exactly_long(socket_fd, REJECTION_TYPE_WRONG_NAME);
        free(filename);
        return false;
    }

    char* complete_filename = malloc(filename_length + strlen(path) + 1);
    if (complete_filename == NULL) {
        free(filename);
        return false;
    }
    strcpy(complete_filename, path);
    strcat(complete_filename, filename);

    FILE* file = fopen(complete_filename, "rb");
    if (file == NULL) {

        free(filename);
        free(complete_filename);
        return false;
    }
    fseek(file, 0, SEEK_END);
    long real_length = ftell(file);
    rewind(file);

    if (real_length == 0 || fragment_start > real_length - 1) {
        printf("client tried to request a file fragment starting past its end \n");
        write_exactly_short(socket_fd, RESPONSE_TYPE_REJECT);
        write_exactly_long(socket_fd, REJECTION_TYPE_WRONG_START);
        fclose(file);
        free(filename);
        free(complete_filename);
        return false;
    }

    off_t length_to_send = real_length < fragment_length ? real_length : fragment_length;

    if (length_to_send <= 0) {
        printf("client tried to request a fragment of wrong length\n");
        write_exactly_short(socket_fd, RESPONSE_TYPE_REJECT);
        write_exactly_long(socket_fd, REJECTION_TYPE_WRONG_LENGTH);
        fclose(file);
        free(filename);
        free(complete_filename);
        return false;
    }

    if (!write_exactly_short(socket_fd, RESPONSE_TYPE_FRAGMENT)
        || !write_exactly_long(socket_fd, length_to_send)
        || !send_file(socket_fd, file, length_to_send, fragment_start, BUFFER_SIZE)) {
        fclose(file);
        free(filename);
        free(complete_filename);
        return false;
    }

    fclose(file);
    free(filename);
    free(complete_filename);
    return true;
}

int main(int argc, char* argv[]) {

    if (argc < 2 || argc > 3) {
        fatal("usage: %s <path to shared directory> [<port number>]", argv[0]);
    }

    int port_num = (argc == 3) ? atoi(argv[2]) : PORT_NUM;

    char path[MAX_PATH];
    strcpy(path, argv[1]);
    if (path[strlen(path) - 1] != '/') {
        strcat(path, "/");
    }

    DIR* dir = opendir(path);
    if (!dir) {
        fatal("given directory does not exist");
    }
    closedir(dir);

    int listening_socket_fd, socket_fd;
    struct sockaddr_in server_address;
    struct sockaddr_in client_address;
    socklen_t client_address_len;
    listening_socket_fd = socket(PF_INET, SOCK_STREAM, 0); // creating IPv4 TCP socket
    if (listening_socket_fd < 0) {
        syserr("socket");
    }

    server_address.sin_family = AF_INET; // IPv4
    server_address.sin_addr.s_addr = htonl(INADDR_ANY); // listening on all interfaces
    server_address.sin_port = htons(port_num); // listening on port PORT_NUM

    // bind the socket to a concrete address
    if (bind(listening_socket_fd, (struct sockaddr*) &server_address, sizeof(server_address)) < 0) {
        syserr("bind");
    }

    // switch to listening (passive open)
    if (listen(listening_socket_fd, QUEUE_LENGTH) < 0) {
        syserr("listen");
    }

    printf("accepting client connections on port %hu\n", ntohs(server_address.sin_port));
    for (;;) {
        client_address_len = sizeof(client_address);
        // get client connection from the socket
        socket_fd = accept(listening_socket_fd, (struct sockaddr*) &client_address, &client_address_len);
        if (socket_fd < 0) {
            log_err("accept");
            continue;
        }
        printf("accepted connection\n");
        bool handled = false;
        while (!handled) {
            uint16_t request_type;
            if (!read_exactly_short(socket_fd, &request_type)) {
                log_err("partial / failed read");
                handled = true;
            }
            printf("received request of type %u\n", request_type);
            if (request_type == REQUEST_TYPE_LIST) {
                if (!handle_filenames_request(socket_fd, path)) {
                    handled = true;
                    log_err("some error");
                }
            } else if (request_type == REQUEST_TYPE_FRAGMENT) {
                if (!handle_fragment_request(socket_fd, path)) {
                    log_err("some error");
                    close(socket_fd);
                }
                handled = true;
            } else {
                log_err("unexpected request type");
                handled = true;
            }
            printf("request handled\n");
        }
        printf("client handled\n");
    }
}