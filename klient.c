#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <malloc.h>


#include "netstore.h"
#include "err.h"

#define TMP_DIR_NAME "tmp"
#define TMP_DIR_MODE 0700

int main(int argc, char* argv[]) {
    int socket_fd;
    struct addrinfo addr_hints;
    struct addrinfo* addr_result;

    int err;
    if (argc != 3) {
        fatal("Usage: %s host port\n", argv[0]);
    }

    // 'converting' host/port in string to struct addrinfo
    memset(&addr_hints, 0, sizeof(struct addrinfo));
    addr_hints.ai_family = AF_INET; // IPv4
    addr_hints.ai_socktype = SOCK_STREAM;
    addr_hints.ai_protocol = IPPROTO_TCP;

    err = getaddrinfo(argv[1], argv[2], &addr_hints, &addr_result);
    if (err == EAI_SYSTEM) { // system error
        syserr("getaddrinfo: %s", gai_strerror(err));
    } else if (err != 0) { // other error (host not found, etc.)
        fatal("getaddrinfo: %s", gai_strerror(err));
    }

    // initialize socket according to getaddrinfo results
    socket_fd = socket(addr_result->ai_family, addr_result->ai_socktype, addr_result->ai_protocol);
    if (socket_fd < 0) {
        syserr("socket");
    }

    // connect socket to the server
    if (connect(socket_fd, addr_result->ai_addr, addr_result->ai_addrlen) < 0) {
        syserr("connect");
    }

    freeaddrinfo(addr_result);

    printf("sending request\n");
    // send file list request
    if (!write_exactly_short(socket_fd, REQUEST_TYPE_LIST)) {
        syserr("partial / failed write");
    }

    printf("sent request\n");

    uint16_t response_type;
    if (!read_exactly_short(socket_fd, &response_type)) {
        syserr("partial / failed read");
    }

    printf("received response of type %hu\n", response_type);

    uint32_t filenames_length;
    if (!read_exactly_long(socket_fd, &filenames_length)) { syserr("partial / failed read"); }

    if (filenames_length > 0) {
        char* filenames = malloc(filenames_length + 1);
        if (filenames == NULL) {
            syserr("malloc failure");
        }

        if (!read_exactly(socket_fd, filenames, filenames_length)) {
            free(filenames);
            syserr("partial / failed read");
        }
        filenames[filenames_length] = '\0';

        size_t files_count = 1;
        for (size_t i = 0; i < filenames_length; ++i) {
            if (filenames[i] == '|') {
                files_count++;
            }
        }
        char* filename_beginnings[files_count];
        filename_beginnings[0] = filenames;
        size_t file_counter = 1;

        for (size_t i = 1; i < filenames_length; ++i) {
            if (filenames[i - 1] == '|') {
                filename_beginnings[file_counter] = filenames + i;
                ++file_counter;
                filenames[i - 1] = '\0';
            }
        }

        for (size_t counter = 1; counter <= files_count; ++counter) {
            printf("%lu. %s\n", counter, filename_beginnings[counter - 1]);
        }

        uint16_t chosen_file;
        printf("Please enter the number of file you'd like to download.\n");
        scanf("%hu", &chosen_file);
        if (chosen_file < 1 || chosen_file > files_count) {
            free(filenames);
            fatal("Incorrect number");
        }
        printf("Please enter the start address.\n");
        uint32_t fragment_start;
        scanf("%u", &fragment_start);
        printf("Please enter the end address.\n");
        uint32_t fragment_end;
        scanf("%u", &fragment_end);
        if (fragment_end < fragment_start) {
            fatal("Incorrect length");
        }
        char* filename = filename_beginnings[chosen_file - 1];
        uint16_t filename_length = strlen(filename);

        if (!write_exactly_short(socket_fd, REQUEST_TYPE_FRAGMENT)
            || !write_exactly_long(socket_fd, fragment_start)
            || !write_exactly_long(socket_fd, fragment_end - fragment_start)
            || !write_exactly_short(socket_fd, filename_length)
            || !write_exactly(socket_fd, filename, filename_length)) {
            free(filenames);
            syserr("partial / failed write");
        }

        uint32_t received_length;

        if (!read_exactly_short(socket_fd, &response_type)
            || !read_exactly_long(socket_fd, &received_length)) {
            free(filenames);
            syserr("partial / failed read");
        }

        if (response_type == RESPONSE_TYPE_REJECT) {
            free(filenames);
            printf("Request was rejected. Reason: ");
            switch (received_length) {
                case REJECTION_TYPE_WRONG_LENGTH:
                    printf("wrong length\n");
                    break;
                case REJECTION_TYPE_WRONG_NAME:
                    printf("wrong name\n");
                    break;
                case REJECTION_TYPE_WRONG_START:
                    printf("wrong start\n");
                    break;
                default:
                    printf("unknown\n");
                    break;
            }
            fatal("Rejected");

        }

        printf("got response of type %u\n", response_type);
        struct stat st = {0};

        char* tmp_dir_name = TMP_DIR_NAME;
        if (stat(tmp_dir_name, &st) == -1) {
            printf("creating tmp directory\n");
            if (mkdir(tmp_dir_name, TMP_DIR_MODE) < 0) {
                free(filenames);
                syserr("unable to create tmp directory");
            }
        }

        char* real_filename = malloc(filename_length + 5);
        if (!real_filename) {
            free(filenames);
            syserr("malloc failure");
        }
        strcpy(real_filename, "tmp/");
        strcat(real_filename, filename);

        printf("opening file\n");
        FILE* file = fopen(real_filename, "r+b");
        if (!file) {
            file = fopen(real_filename, "wb");
            if (!file) {
                free(filenames);
                free(real_filename);
                syserr("Could neither not open file nor create it");
            }
        }
        if (!read_file(socket_fd, file, received_length, fragment_start, BUFFER_SIZE)) { printf("partial / failed read"); }
        printf("file received\n");
        fclose(file);
        free(filenames);
        free(real_filename);
    }

    if (close(socket_fd) < 0) // socket would be closed anyway when the program ends
        syserr("close");

    return 0;
}
