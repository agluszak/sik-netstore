CC = cc
LFLAGS= -Wall

all: netstore-server netstore-client

netstore-server: err.o netstore.o serwer.c
	$(CC) $(LFLAGS) serwer.c err.o netstore.o -o netstore-server

netstore-client: err.o netstore.o klient.c
	$(CC) $(LFLAGS) klient.c err.o netstore.o -o netstore-client

err.o: err.c err.h
	$(CC) $(LFLAGS) err.c -c

netstore.o: netstore.c netstore.h
	$(CC) $(LFLAGS) netstore.c -c

.PHONY: clean

clean:
	rm -f netstore-server netstore-client *.o *~ *.bak
