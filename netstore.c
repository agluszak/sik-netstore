#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>


#include "netstore.h"
#include "err.h"

bool write_exactly(int fd, const void* data, size_t expected_length) {
    ssize_t written_now, already_written, remains;
    already_written = 0; // number of bytes already in the buffer
    do {
        remains = expected_length - already_written; // number of bytes to be read
        written_now = send(fd, data + already_written, remains, MSG_NOSIGNAL);
        if (written_now < 0)
            return false;
        else if (written_now > 0) {
            already_written += written_now;

            if (already_written == expected_length) {
                return true;
            }
        }
    } while (written_now > 0);
    return already_written == expected_length;
}

bool write_exactly_short(int fd, uint16_t num) {
    uint16_t num_network = htons(num);
    return write_exactly(fd, &num_network, sizeof(num_network));
}

bool write_exactly_long(int fd, uint32_t num) {
    uint32_t num_network = htonl(num);
    return write_exactly(fd, &num_network, sizeof(num_network));
}

bool send_file(int out_fd, FILE* file, long size, long offset, long buffer_size) {
    char buffer[buffer_size];
    long processed = 0;
    if (fseek(file, offset, SEEK_SET) != 0)
        return false;
    while (processed != size) {
        long need_to_read = (size - processed) < buffer_size ? size - processed : buffer_size;
        long read_now = fread(buffer, 1, need_to_read, file);
        write_exactly(out_fd, buffer, read_now);
        processed += read_now;
        if (ferror(file) || feof(file)) {
            break;
        }
    }

    return processed == size && !ferror(file);
}

bool read_exactly(int fd, void* data, size_t expected_length) {
    ssize_t read_now, already_read, remains;
    already_read = 0; // number of bytes already in the buffer
    do {
        remains = expected_length - already_read; // number of bytes to be read
        read_now = read(fd, data + already_read, remains);
        if (read_now < 0)
            return false;
        else if (read_now > 0) {
            already_read += read_now;

            if (already_read == expected_length) {
                return true;
            }
        }
    } while (read_now > 0);
    return already_read == expected_length;
}

bool read_exactly_short(int fd, uint16_t* num) {
    bool success = read_exactly(fd, num, sizeof(uint16_t));
    *num = ntohs(*num);
    return success;
}

bool read_exactly_long(int fd, uint32_t* num) {
    bool success = read_exactly(fd, num, sizeof(uint32_t));
    *num = ntohl(*num);
    return success;
}

bool read_file(int in_fd, FILE* file, long size, long offset, long buffer_size) {
    char buffer[buffer_size];
    long processed = 0;
    if (fseek(file, offset, SEEK_SET) != 0)
        return false;
    while (processed != size) {
        long read_now = (size - processed) < buffer_size ? size - processed : buffer_size;

        if (!read_exactly(in_fd, buffer, read_now)) {
            return false;
        }
        fwrite(buffer, 1, read_now, file);
        processed += read_now;
        if (ferror(file) || feof(file)) {
            break;
        }
    }

    return processed == size && !ferror(file);
}