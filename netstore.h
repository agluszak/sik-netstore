#ifndef NETSTORE_H
#define NETSTORE_H

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

#define RESPONSE_TYPE_LIST 1
#define RESPONSE_TYPE_REJECT 2
#define RESPONSE_TYPE_FRAGMENT 3

#define REJECTION_TYPE_WRONG_NAME 1
#define REJECTION_TYPE_WRONG_START 2
#define REJECTION_TYPE_WRONG_LENGTH 3

#define REQUEST_TYPE_LIST 1
#define REQUEST_TYPE_FRAGMENT 2

#define BUFFER_SIZE 512 * 1024

bool write_exactly(int fd, const void* data, size_t expected_length);

bool write_exactly_short(int fd, uint16_t num);

bool write_exactly_long(int fd, uint32_t num);

bool send_file(int out_fd, FILE* file, long size, long offset, long buffer_size);

bool read_exactly(int fd, void* data, size_t expected_length);

bool read_exactly_short(int fd, uint16_t* num);

bool read_exactly_long(int fd, uint32_t* num);

bool read_file(int in_fd, FILE* file, long size, long offset, long buffer_size);

#endif //NETSTORE_H
