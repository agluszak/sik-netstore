#ifndef ERR_H
#define ERR_H

extern void log_err(const char* fmt, ...);

extern void syserr(const char* fmt, ...);

extern void fatal(const char* fmt, ...);

#endif //ERR_H
